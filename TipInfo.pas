unit TipInfo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, PNGImage,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TFTipInfo = class(TForm)
    bg: TImage;
    Image1: TImage;
    Label3: TLabel;
    Image2: TImage;
    procedure FormCreate(Sender: TObject);
    procedure Image1MouseEnter(Sender: TObject);
    procedure Image1MouseLeave(Sender: TObject);
    procedure Image2MouseEnter(Sender: TObject);
    procedure Image2MouseLeave(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FTipInfo: TFTipInfo;

implementation

{$R *.dfm}

uses DataTypes, CustomVCL, Main;

procedure TFTipInfo.Image1MouseEnter(Sender: TObject);
begin Image1.Picture.LoadFromFile('img\1infoA2.png') end;

procedure TFTipInfo.Image1MouseLeave(Sender: TObject);
begin Image1.Picture.LoadFromFile('img\0infoA2.png') end;

procedure TFTipInfo.Image2MouseEnter(Sender: TObject);
begin Image2.Picture.LoadFromFile('img\1infoB2.png') end;

procedure TFTipInfo.Image2MouseLeave(Sender: TObject);
begin Image2.Picture.LoadFromFile('img\0infoB2.png') end;

procedure TFTipInfo.FormCreate(Sender: TObject);
begin
	Image1.Picture.RegisterFileFormat('.PNG', 'PNG Image', TPNGImage);
	Image2.Picture.RegisterFileFormat('.PNG', 'PNG Image', TPNGImage);
  with bg.Canvas do begin Brush.Color:=FormColor; Pen.Color:=$00C6D7DF; Pen.Width:=10; Rectangle(0, 0, bg.Width, bg.Height) end;
  Image1MouseLeave(bg); Image2MouseLeave(bg)
end;

procedure TFTipInfo.Image1Click(Sender: TObject);
begin
  TipInformatie:='A'; FTipInfo.Close; FMain.RefreshInformation
end;

procedure TFTipInfo.Image2Click(Sender: TObject);
begin
  TipInformatie:='B'; FTipInfo.Close; FMain.RefreshInformation
end;

end.
