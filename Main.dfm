object FMain: TFMain
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'FMain'
  ClientHeight = 679
  ClientWidth = 877
  Color = 12298658
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  ScreenSnap = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 21
  object CheckBoxImg: TImage
    Left = 324
    Top = 336
    Width = 200
    Height = 24
    Visible = False
    OnClick = CheckBoxImgClick
    OnMouseEnter = CheckBoxImgMouseEnter
    OnMouseLeave = CheckBoxImgMouseLeave
  end
  object Label2: TLabel
    Left = 308
    Top = 464
    Width = 46
    Height = 21
    Caption = 'Label2'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label7: TLabel
    Left = 310
    Top = 491
    Width = 74
    Height = 32
    Caption = 'Label7'
    Font.Charset = ANSI_CHARSET
    Font.Color = 14937840
    Font.Height = -24
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Shape1: TShape
    Left = 376
    Top = 219
    Width = 65
    Height = 65
    Brush.Color = 2558622
    Visible = False
  end
  object HeaderImg: TImage
    Left = 240
    Top = 92
    Width = 201
    Height = 32
    Cursor = crHandPoint
    OnClick = HeaderImgClick
    OnMouseLeave = HeaderImgMouseLeave
    OnMouseMove = HeaderImgMouseMove
  end
  object PopL: TLabel
    Left = 640
    Top = 472
    Width = 120
    Height = 21
    Alignment = taCenter
    AutoSize = False
    Caption = '19,062,500'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object RowImg: TImage
    Left = 324
    Top = 366
    Width = 200
    Height = 24
    Visible = False
    OnMouseEnter = RowImgMouseEnter
    OnMouseLeave = RowImgMouseLeave
  end
  object ComunaL: TLabel
    Left = 470
    Top = 520
    Width = 67
    Height = 21
    Alignment = taCenter
    Caption = 'ComunaL'
    Font.Charset = ANSI_CHARSET
    Font.Color = 15658734
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object OrasL: TLabel
    Left = 470
    Top = 493
    Width = 41
    Height = 21
    Alignment = taCenter
    Caption = 'OrasL'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object MunicipiuL: TLabel
    Left = 470
    Top = 464
    Width = 89
    Height = 23
    Alignment = taCenter
    Caption = 'MunicipiuL'
    Font.Charset = ANSI_CHARSET
    Font.Color = 15921130
    Font.Height = -17
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object JudetL: TLabel
    Left = 470
    Top = 433
    Width = 59
    Height = 25
    Alignment = taCenter
    Caption = 'JudetL'
    Font.Charset = ANSI_CHARSET
    Font.Color = 15203838
    Font.Height = -19
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object HeaderL: TLabel
    Left = 470
    Top = 397
    Width = 84
    Height = 27
    Alignment = taCenter
    Caption = 'HeaderL'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -21
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object NrL: TLabel
    Left = 470
    Top = 547
    Width = 125
    Height = 21
    Alignment = taCenter
    Caption = 'NrL 0123456789.'
    Font.Charset = ANSI_CHARSET
    Font.Color = 14608103
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object Label7b: TLabel
    Left = 310
    Top = 520
    Width = 60
    Height = 21
    Caption = 'Label7b'
    Font.Charset = ANSI_CHARSET
    Font.Color = 14937840
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LeftPanel: TPanel
    Left = 0
    Top = 0
    Width = 240
    Height = 671
    BevelOuter = bvNone
    TabOrder = 0
    object CloseB: TImage
      Left = 20
      Top = 629
      Width = 200
      Height = 56
      Cursor = crHandPoint
      Hint = #206'nchide'
      OnClick = CloseBClick
      OnMouseEnter = CloseBMouseEnter
      OnMouseLeave = CloseBMouseLeave
    end
    object Label8: TLabel
      Left = 20
      Top = 586
      Width = 218
      Height = 37
      Cursor = crHandPoint
      Caption = #9827' Despre program'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -27
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      OnClick = Label8Click
    end
    object Panel1: TPanel
      Left = 0
      Top = 160
      Width = 240
      Height = 270
      BevelOuter = bvNone
      Color = 11376269
      ParentBackground = False
      TabOrder = 0
      object Label1: TLabel
        Left = 20
        Top = -7
        Width = 79
        Height = 37
        Caption = #9679' Filtre'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Segoe UI Light'
        Font.Style = []
        ParentFont = False
      end
      object ch1: TImage
        Left = 20
        Top = 48
        Width = 200
        Height = 24
        Cursor = crHandPoint
        Hint = '0Romania'
        OnClick = CheckBoxImgClick
        OnMouseEnter = CheckBoxImgMouseEnter
        OnMouseLeave = CheckBoxImgMouseLeave
      end
      object ch2: TImage
        Left = 20
        Top = 100
        Width = 200
        Height = 24
        Cursor = crHandPoint
        Hint = '1Jude'#539'e'
        OnClick = CheckBoxImgClick
        OnMouseEnter = CheckBoxImgMouseEnter
        OnMouseLeave = CheckBoxImgMouseLeave
      end
      object ch3: TImage
        Left = 20
        Top = 182
        Width = 200
        Height = 24
        Cursor = crHandPoint
        Hint = '0Municipii'
        OnClick = CheckBoxImgClick
        OnMouseEnter = CheckBoxImgMouseEnter
        OnMouseLeave = CheckBoxImgMouseLeave
      end
      object ch4: TImage
        Left = 20
        Top = 208
        Width = 200
        Height = 24
        Cursor = crHandPoint
        Hint = '0Ora'#537'e'
        OnClick = CheckBoxImgClick
        OnMouseEnter = CheckBoxImgMouseEnter
        OnMouseLeave = CheckBoxImgMouseLeave
      end
      object ch5: TImage
        Left = 20
        Top = 234
        Width = 200
        Height = 24
        Cursor = crHandPoint
        Hint = '0Comune'
        OnClick = CheckBoxImgClick
        OnMouseEnter = CheckBoxImgMouseEnter
        OnMouseLeave = CheckBoxImgMouseLeave
      end
      object SelJudB: TImage
        Left = 20
        Top = 156
        Width = 200
        Height = 24
        Cursor = crHandPoint
        Hint = 'Filtru jude'#539'e'
        OnClick = SelJudBClick
        OnMouseEnter = SelJudBMouseEnter
        OnMouseLeave = SelJudBMouseLeave
      end
      object Label11: TLabel
        Left = 36
        Top = 25
        Width = 108
        Height = 23
        Caption = #9676' Entit'#259#539'i mari'
        Font.Charset = ANSI_CHARSET
        Font.Color = 14211288
        Font.Height = -17
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 36
        Top = 130
        Width = 83
        Height = 23
        Caption = #9676' Localit'#259#539'i'
        Font.Charset = ANSI_CHARSET
        Font.Color = 14211288
        Font.Height = -17
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object SelRegB: TImage
        Left = 20
        Top = 74
        Width = 200
        Height = 24
        Cursor = crHandPoint
        Hint = 'Regiuni'
        OnMouseEnter = SelJudBMouseEnter
        OnMouseLeave = SelJudBMouseLeave
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 240
      Height = 161
      BevelOuter = bvNone
      Color = 11376269
      ParentBackground = False
      TabOrder = 1
      object Label3: TLabel
        Left = 20
        Top = 8
        Width = 114
        Height = 37
        Caption = #9827' Op'#539'iuni'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 20
        Top = 40
        Width = 171
        Height = 37
        Caption = #9679' Tip informa'#539'ii'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Segoe UI Light'
        Font.Style = []
        ParentFont = False
      end
      object TipInfoImg: TImage
        Left = 20
        Top = 83
        Width = 200
        Height = 64
        Cursor = crHandPoint
        Center = True
        OnClick = TipInfoImgClick
        OnMouseEnter = TipInfoImgMouseEnter
        OnMouseLeave = TipInfoImgMouseLeave
      end
      object tempExportB: TButton
        Left = 140
        Top = 20
        Width = 94
        Height = 25
        Caption = 'tempExportB'
        TabOrder = 0
        Visible = False
        OnClick = tempExportBClick
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 430
      Width = 240
      Height = 150
      BevelOuter = bvNone
      Color = 11376269
      ParentBackground = False
      TabOrder = 2
      object Label10: TLabel
        Left = 20
        Top = -2
        Width = 169
        Height = 37
        Caption = #9827' Info entitate'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object InfoImg: TImage
        Left = 20
        Top = 41
        Width = 200
        Height = 100
      end
    end
  end
  object TopPanel: TPanel
    Left = 240
    Top = 0
    Width = 433
    Height = 92
    BevelOuter = bvNone
    Color = 11376269
    ParentBackground = False
    TabOrder = 1
    object Label9: TLabel
      Left = 0
      Top = 8
      Width = 181
      Height = 37
      Caption = #9827' Derulare list'#259
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -27
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Panel3: TPanel
      Left = 0
      Top = 51
      Width = 339
      Height = 24
      BevelOuter = bvNone
      Color = clSilver
      ParentBackground = False
      TabOrder = 0
      object Label5: TLabel
        Left = 0
        Top = 1
        Width = 80
        Height = 21
        Cursor = crHandPoint
        Alignment = taCenter
        AutoSize = False
        Caption = 'Label5'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
        OnMouseEnter = Label5MouseEnter
        OnMouseLeave = Label5MouseLeave
      end
      object Label6: TLabel
        Left = 264
        Top = 1
        Width = 80
        Height = 21
        Cursor = crHandPoint
        Alignment = taCenter
        AutoSize = False
        Caption = 'Label6'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
        OnMouseEnter = Label6MouseEnter
        OnMouseLeave = Label6MouseLeave
      end
      object BarImg: TImage
        Left = 80
        Top = 0
        Width = 153
        Height = 24
        Cursor = crHandPoint
        OnClick = BarImgClick
        OnMouseEnter = BarImgMouseEnter
        OnMouseLeave = BarImgMouseLeave
        OnMouseMove = BarImgMouseMove
      end
    end
  end
  object MainPanel: TPanel
    Left = 240
    Top = 124
    Width = 185
    Height = 41
    BevelOuter = bvNone
    Color = 11376269
    ParentBackground = False
    TabOrder = 2
  end
  object Panel5: TPanel
    Left = 536
    Top = 208
    Width = 200
    Height = 100
    BevelOuter = bvNone
    Color = 11379597
    ParentBackground = False
    TabOrder = 3
    Visible = False
    object Label13: TLabel
      Left = 4
      Top = 2
      Width = 70
      Height = 25
      Caption = 'Label13'
      Font.Charset = ANSI_CHARSET
      Font.Color = 5522480
      Font.Height = -19
      Font.Name = 'Ubuntu Light'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label14: TLabel
      Left = 6
      Top = 24
      Width = 39
      Height = 13
      Caption = 'Label14'
      Font.Charset = ANSI_CHARSET
      Font.Color = 15790320
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label15: TLabel
      Left = 6
      Top = 38
      Width = 57
      Height = 17
      Caption = 'Popula'#539'ie:'
      Font.Charset = ANSI_CHARSET
      Font.Color = 15329769
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label16: TLabel
      Left = 69
      Top = 38
      Width = 47
      Height = 17
      Caption = 'Label16'
      Font.Charset = ANSI_CHARSET
      Font.Color = 15329769
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
end
