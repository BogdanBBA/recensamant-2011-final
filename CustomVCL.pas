﻿unit CustomVCL;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls;

const
	FormColor = $00ADA38D;
  ControlColor = $00A09D78;
  ControlColorMouse = $00BBBA9F;
  colYes = $0006C898;
  colNo = $00270A9E;
  BarColor = $00C6D7DF;
  BarColorMouse = $00E7EEF1;
  HeaderColor: array[0..1] of TColor = ($003E3E3E, $004B4B4B);
  SortingColor = $0099DDF0;
  RowColor: array[0..1] of TColor = ($00A59981, $00B7AD99);

procedure CheckBoxImageMouseEnter(img: TImage);
procedure CheckBoxImageMouseLeave(img: TImage);
procedure RefreshImageButton(var butt: TImage; fillCol: TColor; smallButton: boolean);
procedure RefreshRowImage(x: ShortInt; MouseOver, TotalsRow: boolean);
procedure FillHeaderInfo(img: TImage; bgCol: TColor; text: string; aFont: TFont; X1, X2: word);
procedure RefreshInfoImg(x: word);

implementation

uses Main, FunctiiBaza, DataTypes;

procedure CheckBoxImageMouseEnter(img: TImage);
begin
  img.Picture:=nil;
  with img.Canvas do
  	begin
    	Brush.Color:=ControlColorMouse; FillRect(Rect(0, 0, img.Width, img.Height));
      if img.Hint[1]='1' then Brush.Color:=colYes else Brush.Color:=colNo; FillRect(Rect(8, 4, 24, 20));
      Pen.Color:=clWhite; Rectangle(8, 4, 24, 20); Brush.Style:=bsClear; Font:=FMain.Label2.Font;
      TextOut(32, 1, copy(img.Hint, 2, 255))
    end
end;

procedure CheckBoxImageMouseLeave(img: TImage);
begin
  img.Picture:=nil;
  with img.Canvas do
  	begin
    	Brush.Color:=FormColor; FillRect(Rect(0, 0, img.Width, img.Height));
      if img.Hint[1]='1' then Brush.Color:=colYes else Brush.Color:=colNo; FillRect(Rect(8, 4, 24, 20));
      Pen.Color:=clBlack; Rectangle(8, 4, 24, 20); Brush.Style:=bsClear; Font:=FMain.Label2.Font;
      TextOut(32, 1, copy(img.Hint, 2, 255))
    end
end;

procedure RefreshImageButton(var butt: TImage; fillCol: TColor; smallButton: boolean);
var tw, th: integer;
begin
  butt.Picture:=nil;
  with butt.Canvas do
  	begin
		  if smallButton then begin Pen.Color:=$004D4D4D; Font:=FMain.Label7b.Font end
      else begin Pen.Color:=clBlack; Font:=FMain.Label7.Font end;
      Brush.Color:=fillCol; Pen.Width:=1; Rectangle(0, 0, butt.Width, butt.Height);
      tw:=TextWidth(butt.Hint); th:=TextHeight(butt.Hint);
		  TextOut(butt.Width div 2-tw div 2, butt.Height div 2-th div 2, butt.Hint)
    end
end;

procedure FillHeaderInfo(img: TImage; bgCol: TColor; text: string; aFont: TFont; X1, X2: word);
begin
	with img.Canvas do
  	begin
      Brush.Color:=bgCol; FillRect(Rect(X1, 0, X2, img.Height)); Font:=aFont; Font.Style:=[fsBold];
      if text=SortingCriteria then Font.Color:=SortingColor;
      TextOut(X1+(X2-X1) div 2-TextWidth(text) div 2, img.Height div 2-TextHeight(text) div 2, text)
    end
end;

procedure FillRowImageInfo(img: TImage; bgCol: TColor; text: string; aFont: TFont; X1, X2: word);
begin
	with img.Canvas do
  	begin
      Brush.Color:=bgCol; FillRect(Rect(X1, 0, X2, img.Height)); Font:=aFont;
      TextOut(X1+(X2-X1) div 2-TextWidth(text) div 2, img.Height div 2-TextHeight(text) div 2, text)
    end
end;

procedure RefreshRowImage(x: ShortInt; MouseOver, TotalsRow: boolean);
var c: TColor; p: TPopulationEntity; txt: string; f: TFont; nextLeft, i: word;
begin
	if TotalsRow then c:=HeaderColor[Integer(MouseOver)] else c:=RowColor[Integer(Odd(x))];
  if MouseOver and (not TotalsRow) then c:=RGB(GetRValue(c)+24, GetGValue(c)+24, GetBValue(c)+24);
  Row[x].Picture:=nil; Row[x].Canvas.Brush.Color:=c; Row[x].Canvas.FillRect(Rect(0, 0, Row[x].Width, 24));
  if not TotalsRow then if (not (ListStart+x in [0..List.Count-1])) or (ListStart=-1) then Exit;
  if TotalsRow then p:=d.SelData else p:=TPopulationEntity(List[ListStart+x]);
  nextLeft:=0;
  for i:=0 to SelCol.Count-1 do
  	begin
      case StrToCase(SelCol[i], Column) of
      -1: Continue;
      0: begin f:=FMain.NrL.Font; if TotalsRow then txt:=Format('∑(%d)', [List.Count]) else txt:=IntToStr(ListStart+x+1) end;
      1: begin
           case p.Tip of
           0..2, 7: f:=FMain.JudetL.Font;
           3: f:=FMain.MunicipiuL.Font;
           4, 6: f:=FMain.OrasL.Font;
           5: f:=FMain.ComunaL.Font;
           end;
      		 txt:=p.Nume
      	 end;
      2: begin f:=FMain.HeaderL.Font; f.Style:=[]; txt:=OtherStr end;
      3: begin f:=FMain.PopL.Font; f.Style:=[fsBold]; txt:=Grupat(p.GetPopulation(SelCol[i])) end;
      else
      	begin
        	f:=FMain.PopL.Font; if TotalsRow then f.Style:=[fsBold] else f.Style:=[];
          if p.GetAsterisk(SelCol[i]) then txt:='*'
          else if p.GetPopulation(SelCol[i])=0 then txt:='-'
          else txt:=Grupat(p.GetPopulation(SelCol[i]))
        end;
      end;
    	if TotalsRow then begin FillRowImageInfo(Row[NRows], c, txt, f, nextLeft, nextLeft+SelColW[i]); Inc(nextLeft, SelColW[i]) end
      else begin FillRowImageInfo(Row[x], c, txt, f, nextLeft, nextLeft+SelColW[i]); Inc(nextLeft, SelColW[i]) end
    end
end;

procedure RefreshInfoImg(x: word);
var p: TPopulationEntity; sx: string;
begin
  if (not (ListStart+x in [0..List.Count-1])) or (ListStart=-1) then Exit;
  p:=TPopulationEntity(List[ListStart+x]);
  FMain.InfoImg.Picture:=nil;
  with FMain.InfoImg.Canvas do
  	begin
    	Brush.Color:=FormColor; FillRect(Rect(0, 0, FMain.InfoImg.Width, FMain.InfoImg.Height));
      Font:=FMain.Label13.Font; TextOut(4, 2, p.Nume);
      sx:=TipPopulation[p.Tip]; if p.Tip in [3..5] then try sx:=sx+' în județul '+d.GetJudetForLocality(p.Nume).Nume except end;
			Font:=FMain.Label14.Font; TextOut(4, 24, sx);
			Font:=FMain.Label15.Font; TextOut(4, 38, 'Populație:');
			Font:=FMain.Label16.Font; TextOut(65, 38, Grupat(p.GetPopulation(0)));
    end
end;

end.
