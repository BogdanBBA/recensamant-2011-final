unit About;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, PNGImage,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TFAbout = class(TForm)
    bg: TImage;
    Label3: TLabel;
    Label4: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    CloseB: TImage;
    procedure FormCreate(Sender: TObject);
    procedure CloseBMouseEnter(Sender: TObject);
    procedure CloseBMouseLeave(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CloseBClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FAbout: TFAbout;

implementation

{$R *.dfm}

uses CustomVCL;

procedure TFAbout.CloseBClick(Sender: TObject);
begin
	FAbout.Close
end;

procedure TFAbout.CloseBMouseEnter(Sender: TObject);
begin RefreshImageButton(TImage(Sender), ControlColorMouse, false) end;

procedure TFAbout.CloseBMouseLeave(Sender: TObject);
begin RefreshImageButton(TImage(Sender), ControlColor, false) end;

procedure TFAbout.FormCreate(Sender: TObject);
begin
  with bg.Canvas do begin Brush.Color:=FormColor; Pen.Color:=$00C6D7DF; Pen.Width:=10; Rectangle(0, 0, bg.Width, bg.Height) end
end;

procedure TFAbout.FormShow(Sender: TObject);
begin
	CloseB.OnMouseLeave(CloseB)
end;

end.
