﻿unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, PNGImage,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TFMain = class(TForm)
    LeftPanel: TPanel;
    Panel1: TPanel;
    Label1: TLabel;
    ch1: TImage;
    ch2: TImage;
    ch3: TImage;
    CheckBoxImg: TImage;
    ch4: TImage;
    Label2: TLabel;
    Panel2: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    TipInfoImg: TImage;
    TopPanel: TPanel;
    MainPanel: TPanel;
    Panel3: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    BarImg: TImage;
    CloseB: TImage;
    Label7: TLabel;
    Shape1: TShape;
    HeaderImg: TImage;
    PopL: TLabel;
    RowImg: TImage;
    ComunaL: TLabel;
    OrasL: TLabel;
    MunicipiuL: TLabel;
    JudetL: TLabel;
    HeaderL: TLabel;
    NrL: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Panel4: TPanel;
    Label10: TLabel;
    ch5: TImage;
    SelJudB: TImage;
    Label7b: TLabel;
    InfoImg: TImage;
    Label11: TLabel;
    Label12: TLabel;
    SelRegB: TImage;
    Panel5: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    tempExportB: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CheckBoxImgMouseEnter(Sender: TObject);
    procedure CheckBoxImgMouseLeave(Sender: TObject);
    procedure CheckBoxImgClick(Sender: TObject);
    procedure TipInfoImgMouseEnter(Sender: TObject);
    procedure TipInfoImgMouseLeave(Sender: TObject);
    procedure TipInfoImgClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BarImgMouseEnter(Sender: TObject);
    procedure BarImgMouseLeave(Sender: TObject);
    procedure CloseBMouseEnter(Sender: TObject);
    procedure CloseBMouseLeave(Sender: TObject);
    procedure CloseBClick(Sender: TObject);
    procedure RowImgMouseLeave(Sender: TObject);
    procedure RowImgMouseEnter(Sender: TObject);
    procedure BarImgClick(Sender: TObject);
    procedure BarImgMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure HeaderImgMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure Label6MouseEnter(Sender: TObject);
    procedure Label6MouseLeave(Sender: TObject);
    procedure Label5MouseEnter(Sender: TObject);
    procedure Label5MouseLeave(Sender: TObject);
    procedure HeaderImgMouseLeave(Sender: TObject);
    procedure HeaderImgClick(Sender: TObject);
    procedure Label8Click(Sender: TObject);
    procedure SelJudBMouseEnter(Sender: TObject);
    procedure SelJudBMouseLeave(Sender: TObject);
    procedure SelJudBClick(Sender: TObject);
    procedure tempExportBClick(Sender: TObject);
  private
    { Private declarations }
  public
  	procedure RecompileList;
    procedure RefreshInformation;
  end;

var
  FMain: TFMain;
  NRows: byte;
  Row: array of TImage;
  BarImgX, HeaderMouseSection: Integer;

implementation

{$R *.dfm}

uses DataTypes, Convertor, FunctiiBaza, CustomVCL, TipInfo, UIntList, About, SelectorJudete, XML.VerySimple;

procedure TFMain.BarImgClick(Sender: TObject);
var mij, jum: real; a, b: word;
begin
	if List.Count=0 then Exit;
	mij:=BarImgX/BarImg.Width; jum:=(NRows/2)/List.Count;
  if BarImgX<BarImg.Width/2 then begin a:=max(0, Round((mij-jum)*List.Count)); b:=min(List.Count-1, a+NRows-1) end
  else begin b:=min(List.Count-1, Round((mij+jum)*List.Count)); a:=max(0, b-NRows+1) end;
  //showmessagefmt('from A=%d to B=%d, count=%d', [a, b, b-a+1]);
  ListStart:=a; ListEnd:=b; FMain.RefreshInformation; BarImg.OnMouseEnter(BarImg)
end;

procedure TFMain.BarImgMouseEnter(Sender: TObject);
var x1, x2: word;
begin
  if List.Count=0 then begin Label5.Caption:='-'; Label6.Caption:='-' end
  else begin Label5.Caption:=IntToStr(ListStart+1); Label6.Caption:=IntToStr(ListEnd+1)+'/'+IntToStr(List.Count) end;
	BarImg.Picture:=nil;
  with BarImg.Canvas do
  	begin
    	Brush.Color:=FormColor; Pen.Color:=BarColorMouse; Pen.Width:=2; Rectangle(1, 1, BarImg.Width, BarImg.Height);
      if List.Count<>0 then
      	begin
        	x1:=max(3, Round(BarImg.Width*(ListStart/List.Count))); x2:=min(BarImg.Width-3, Round(BarImg.Width*(ListEnd/List.Count)));
          Brush.Color:=BarColorMouse; FillRect(Rect(x1, 3, x2, barImg.Height-3))
        end
  	end
end;

procedure TFMain.BarImgMouseLeave(Sender: TObject);
var x1, x2: word;
begin
  if List.Count=0 then begin Label5.Caption:='-'; Label6.Caption:='-' end
  else begin Label5.Caption:=IntToStr(ListStart+1); Label6.Caption:=IntToStr(ListEnd+1) end;
  Label5.Font.Color:=BarColor; Label6.Font.Color:=BarColor;
	BarImg.Picture:=nil;
  with BarImg.Canvas do
  	begin
    	Brush.Color:=FormColor; Pen.Color:=BarColor; Pen.Width:=2; Rectangle(1, 1, BarImg.Width, BarImg.Height);
      if List.Count<>0 then
      	begin
        	x1:=max(3, Round(BarImg.Width*(ListStart/List.Count))); x2:=min(BarImg.Width-3, Round(BarImg.Width*(ListEnd/List.Count)));
          Brush.Color:=BarColor; FillRect(Rect(x1, 3, x2, barImg.Height-3))
        end
  	end
end;

procedure TFMain.BarImgMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin BarImgX:=X end;

procedure TFMain.CheckBoxImgClick(Sender: TObject);
begin
	with TImage(Sender) do Hint:=IntToStr(integer(not boolean(StrToInt(Hint[1]))))+Copy(Hint, 2, 255);
  if FSelJud.Showing then FSelJud.RefreshSelJudInfo else FMain.RecompileList;
  CheckBoxImgMouseEnter(Sender)
end;

procedure TFMain.CheckBoxImgMouseEnter(Sender: TObject);
begin CheckBoxImageMouseEnter(TImage(Sender)) end;

procedure TFMain.CheckBoxImgMouseLeave(Sender: TObject);
begin CheckBoxImageMouseLeave(TImage(Sender)) end;

procedure TFMain.CloseBClick(Sender: TObject);
begin
	FMain.Close
end;

procedure TFMain.CloseBMouseEnter(Sender: TObject);
begin RefreshImageButton(TImage(Sender), ControlColorMouse, false) end;

procedure TFMain.CloseBMouseLeave(Sender: TObject);
begin RefreshImageButton(TImage(Sender), ControlColor, false) end;

procedure TFMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	SelCol.Free; SelColW.Free; FiltruJudete.Free;
	if List<>nil then List.Free;
	if d<>nil then d.Free
end;

procedure TFMain.FormCreate(Sender: TObject);
var i: word;
begin
	//if ConvertDataFile('FromXLS.txt', 'DataTemporary.xml') then FMain.Caption:='Succes!!!' else FMain.caption:='FAIL :(('
  d:=TCensusData.Create('Data.xml'); List:=TList.Create;
  SelCol:=TStringList.Create; SelCol.Sorted:=false; SelCol.Duplicates:=dupIgnore;
  SelCol.Delimiter:=';'; SelCol.DelimitedText:='Nr.;NUME;TOTAL;Romani;Maghiari;Germani;Romi;"• • •";"Alta etnie"';
  SelColW:=TIntList.Create; SelColW.Sorted:=false; SelColW.Duplicates:=dupAccept; GetSelColWidths;
  FiltruJudete:=TStringList.Create; FiltruJudete.Sorted:=true; FiltruJudete.Duplicates:=dupIgnore;
  if d.nJudete>0 then for i:=0 to d.nJudete-1 do FiltruJudete.Add(d.Judet[i].OwnData.Nume);
  HeaderMouseSection:=-1; SortingCriteria:='NUME'; SortingDescending:=false;
  //
  TipInfoImg.Picture.RegisterFileFormat('.PNG', 'PNG Image', TPNGImage);
  //
	FMain.Color:=FormColor; LeftPanel.Color:=FMain.Color; Panel1.Color:=FMain.Color; Panel2.Color:=FMain.Color;
  TopPanel.Color:=FMain.Color; MainPanel.Color:=FMain.Color; Panel3.Color:=FMain.Color; Panel4.Color:=FMain.Color;
  TipInformatie:='A'; TipInfoImgMouseLeave(FMain);
  ch2.Hint:=Format('1Județe (%d)', [d.nJudete]); ch3.Hint:=Format('0Municipii (%d)', [d.Stats.totalMunicipii]);
  ch4.Hint:=Format('0Orașe (%d)', [d.Stats.totalOrase]); ch5.Hint:=Format('0Comune (%d)', [d.Stats.totalComune]);
  for i:=1 to 5 do CheckBoxImgMouseLeave(FMain.FindComponent('ch'+IntToStr(i)));
  CloseB.OnMouseLeave(CloseB); SelJudB.OnMouseLeave(SelJudB)
end;

procedure TFMain.FormResize(Sender: TObject);
var i: word;
begin
	LeftPanel.Height:=FMain.Height; TopPanel.Width:=FMain.Width-LeftPanel.Width;
  MainPanel.Height:=FMain.Height-MainPanel.Top; MainPanel.Width:=TopPanel.Width; HeaderImg.Width:=TopPanel.Width;
	Panel3.Width:=TopPanel.Width-2*Panel3.Left; Label6.Left:=Panel3.Width-Label6.Width; BarImg.Width:=Panel3.Width-2*Label5.Width;
  CloseB.Top:=LeftPanel.Height-CloseB.Height-24;
	// Image creation designed to run only once
	NRows:=(MainPanel.Height-4) div 24; SetLength(Row, NRows); Inc(NRows, -1);
	for i:=0 to NRows do
  	begin
      Row[i]:=TImage.Create(Self);
      with Row[i] do
      	begin
        	Parent:=MainPanel; Left:=0; Top:=i*24; Width:=MainPanel.Width; Height:=24;
		      OnMouseEnter:=RowImgMouseEnter; OnMouseLeave:=RowImgMouseLeave
        end
    end;
  Row[NRows].Height:=28; GetSelColWidths; FMain.RecompileList
end;

procedure TFMain.HeaderImgClick(Sender: TObject);
begin
	if HeaderMouseSection<>-1 then
  	if (SelCol[HeaderMouseSection]<>OtherStr) and (SelCol[HeaderMouseSection]<>Column[0]) then
    	begin
      	if SortingCriteria=SelCol[HeaderMouseSection] then SortingDescending:=not SortingDescending
        else begin SortingCriteria:=SelCol[HeaderMouseSection]; SortingDescending:=SortingCriteria<>'NUME' end;
        List.Sort(ComparePopulationEntities); FMain.RefreshInformation
      end
end;

procedure TFMain.HeaderImgMouseLeave(Sender: TObject);
var i, nextLeft: integer;
begin
	nextLeft:=0; for i:=0 to HeaderMouseSection-1 do Inc(nextLeft, SelColW[i]);
	if HeaderMouseSection<>-1 then
  	FillHeaderInfo(HeaderImg, HeaderColor[0], SelCol[HeaderMouseSection], HeaderL.Font, nextLeft, nextLeft+SelColW[HeaderMouseSection]);
	HeaderMouseSection:=-1
end;

procedure TFMain.HeaderImgMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var i, r, nextLeft: integer;
begin
	r:=-1; nextLeft:=0;
  for i:=0 to SelCol.Count-1 do
  	if (nextLeft<=X) and (X<nextLeft+SelColW[i]) then begin r:=i; break end
    else Inc(nextLeft, SelColW[i]);
  if (r<>-1) and (r<>HeaderMouseSection) then
  	begin
  		FillHeaderInfo(HeaderImg, HeaderColor[1], SelCol[r], HeaderL.Font, nextLeft, nextLeft+SelColW[r]);
  		if HeaderMouseSection<>-1 then
      	begin
        	nextLeft:=0; for i:=0 to HeaderMouseSection-1 do Inc(nextLeft, SelColW[i]);
	      	FillHeaderInfo(HeaderImg, HeaderColor[0], SelCol[HeaderMouseSection], HeaderL.Font, nextLeft, nextLeft+SelColW[HeaderMouseSection])
        end;
      HeaderMouseSection:=r
    end
end;

procedure TFMain.Label5MouseEnter(Sender: TObject);
begin Label5.Caption:=Format('PREV %d', [NRows]); Label5.Font.Color:=BarColorMouse end;

procedure TFMain.Label5MouseLeave(Sender: TObject);
begin BarImg.OnMouseLeave(BarImg) end;

procedure TFMain.Label6MouseEnter(Sender: TObject);
begin Label6.Caption:=Format('NEXT %d', [NRows]); Label6.Font.Color:=BarColorMouse end;

procedure TFMain.Label6MouseLeave(Sender: TObject);
begin BarImg.OnMouseLeave(BarImg) end;

procedure TFMain.Label8Click(Sender: TObject);
begin FAbout.ShowModal end;

procedure TFMain.TipInfoImgClick(Sender: TObject);
begin FTipInfo.ShowModal end;

procedure TFMain.TipInfoImgMouseEnter(Sender: TObject);
begin TipInfoImg.Picture.LoadFromFile('img\1info'+TipInformatie+'.png') end;

procedure TFMain.TipInfoImgMouseLeave(Sender: TObject);
begin TipInfoImg.Picture.LoadFromFile('img\0info'+TipInformatie+'.png') end;

//

procedure TFMain.RecompileList;
var i, j: word;
begin
	TipInfoImgMouseLeave(TipInfoImg);
  //
  with d.SelStats do begin totalLocalitati:=0; totalMunicipii:=0; totalOrase:=0; totalComune:=0 end;
  //
  List.Clear;
  if TipInformatie='A' then
  	begin
    	if ch1.Hint[1]='1' then
      	List.Add(d.NationWideData);
      if ch2.Hint[1]='1' then
      	if d.nJudete>0 then for i:=0 to d.nJudete-1 do
          List.Add(d.Judet[i].OwnData);
      if d.nJudete>0 then for i:=0 to d.nJudete-1 do
        if FiltruJudete.IndexOf(d.Judet[i].OwnData.Nume)<>-1 then
      		if d.Judet[i].nLocalitati>0 then for j:=0 to d.Judet[i].nLocalitati-1 do
		      	begin
		      		if (ch3.Hint[1]='1') and (d.Judet[i].Localitate[j].Tip=3) then
		          	begin List.Add(d.Judet[i].Localitate[j]); Inc(d.SelStats.totalMunicipii) end;
		      		if (ch4.Hint[1]='1') and (d.Judet[i].Localitate[j].Tip=4) then
        		  	begin List.Add(d.Judet[i].Localitate[j]); Inc(d.SelStats.totalOrase) end;
    		  		if (ch5.Hint[1]='1') and (d.Judet[i].Localitate[j].Tip=5) then
		          	begin List.Add(d.Judet[i].Localitate[j]); Inc(d.SelStats.totalComune) end
		        end
    end;
  //
  SelJudB.Hint:=Format('Filtru județe (%d/%d)', [FiltruJudete.Count, d.nJudete]); SelJudB.OnMouseLeave(SelJudB);
  ch3.Hint:=Format('%sMunicipii (%d/%d)', [ch3.Hint[1], d.SelStats.totalMunicipii, d.Stats.totalMunicipii]); ch3.OnMouseLeave(ch3);
  ch4.Hint:=Format('%sOrașe (%d/%d)', [ch4.Hint[1], d.SelStats.totalOrase, d.Stats.totalOrase]); ch3.OnMouseLeave(ch4);
  ch5.Hint:=Format('%sComune (%d/%d)', [ch5.Hint[1], d.SelStats.totalComune, d.Stats.totalComune]); ch3.OnMouseLeave(ch5);

  d.SelData.InitializeValues;
  if List.Count>0 then for i:=0 to List.Count-1 do
  	d.SelData.AddValuesToCurrent(TPopulationEntity(List[i]));
  //
  if List.Count=0 then begin ListStart:=-1; ListEnd:=-1 end else begin ListStart:=0; ListEnd:=min(NRows-1, List.Count-1) end;
  List.Sort(ComparePopulationEntities);	RefreshInformation
end;

procedure TFMain.RefreshInformation;
var i, nextLeft: word;
begin
	Label9.Caption:=Format('♣ Derulare listă (%d)', [List.Count]);
  BarImg.OnMouseLeave(BarImg);
  //
  HeaderImg.Picture:=nil;
  with HeaderImg.Canvas do
  	begin
      Brush.Color:=clFuchsia; FillRect(Rect(0, 0, HeaderImg.Width, HeaderImg.Height)); nextLeft:=0;
      for i:=0 to SelCol.Count-1 do
      	begin
        	FillHeaderInfo(HeaderImg, HeaderColor[Integer(HeaderMouseSection=i)], SelCol[i], HeaderL.Font, nextLeft, nextLeft+SelColW[i]);
          Inc(nextLeft, SelColW[i])
        end;
    end;
  for i:=0 to NRows-1 do
  	RefreshRowImage(i, false, false);
  RefreshRowImage(NRows, false, true)
end;

procedure TFMain.RowImgMouseEnter(Sender: TObject);
var x, i: ShortInt;
begin
	if Sender=Row[NRows] then begin RefreshRowImage(NRows, true, true); Exit end;
	x:=-1; for i:=0 to NRows-1 do if Row[i]=Sender then begin x:=i; break end; if x=-1 then Exit;
  RefreshRowImage(x, true, false); RefreshInfoImg(x)
end;

procedure TFMain.RowImgMouseLeave(Sender: TObject);
var x, i: ShortInt;
begin
	if Sender=Row[NRows] then begin RefreshRowImage(NRows, false, true); Exit end;
	x:=-1; for i:=0 to NRows-1 do if Row[i]=Sender then begin x:=i; break end; if x=-1 then Exit;
  RefreshRowImage(x, false, false); InfoImg.Picture:=nil
end;

procedure TFMain.SelJudBClick(Sender: TObject);
begin FSelJud.ShowModal end;

procedure TFMain.SelJudBMouseEnter(Sender: TObject);
begin RefreshImageButton(TImage(Sender), ControlColorMouse, true) end;

procedure TFMain.SelJudBMouseLeave(Sender: TObject);
begin RefreshImageButton(TImage(Sender), ControlColor, true) end;

procedure TFMain.tempExportBClick(Sender: TObject);
var xml: TXmlVerySimple; i: word;
begin
	xml:=TXmlVerySimple.Create;
  xml.Root.NodeName:='JudeteRecensamant2011Final';
  for i:=0 to d.nJudete-1 do
  	begin
      xml.Root.AddChild('judet');
      with xml.Root.ChildNodes.Last do
      	begin
          SetAttribute('nume', d.Judet[i].OwnData.Nume);
          SetAttribute('suprafata', '0');
          SetAttribute('populatie', IntToStr(d.Judet[i].OwnData.GetPopulation('TOTAL')));
          SetAttribute('data', '0')
        end;
    end;
  xml.SaveToFile('JudeteExport.xml');
  xml.Free
end;

end.
