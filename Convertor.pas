unit Convertor;		// From "from XLS" file to .XML

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, XML.VerySimple;

var
	XML: TXMLVerySimple;
  judI, locI: integer;
  OraseAcum: boolean;

function ConvertDataFile(inF, outF: string): boolean;

implementation

uses FunctiiBaza;

{
		Prerequisites:
    	- must have a text file with the XLS data (including county name, city and commune list, and the A./B. headers);
      - must have replaced manually in Notepad all Tab spaces with regular space characters;
      - eventual fa mici corectii in fisierul de intrare;
      - corecteaza particularitati (de exemplu, sectoarele bucurestene numerotate cu 1-6 sau localitati care incep cu numere ("23 august"));
      - verifica si eventual corecteaza fisierul .XML rezultat;
}

function GetXLSFileName(x: string): string;
var p: word;
begin
	try p:=1; while not CharInSet(x[p], ['0'..'9']) do inc(p); Result:=copy(x, 1, p-2)
  except Result:='GetXLSFileName(x="'+x+'"): ???' end
end;

procedure GetNewJudet(x: string);
var nume: string;
begin
  Delete(x, 1, 1); nume:=GetXLSFileName(x); Delete(x, 1, length(nume)+1); Delete(x, pos(' '+nume, x), length(nume)+1);
  inc(judI); XML.Root.AddChild('Judet');
  with XML.Root.ChildNodes[judI] do
  	begin SetAttribute('nume', nume); SetAttribute('pop', x) end
end;

procedure PrepareTowns(x: string);
var nume: string;
begin
  nume:=GetXLSFileName(x); Delete(x, 1, length(nume)+1); Delete(x, pos(' '+nume, x), length(nume)+1);
  locI:=-1; OraseAcum:=true;
  XML.Root.ChildNodes[judI].AddChild('Municipii_Orase');
  with XML.Root.ChildNodes[judI].Find('Municipii_Orase') do
  	begin SetAttribute('pop', x) end
end;

procedure PrepareVillages(x: string);
var nume: string;
begin
  nume:=GetXLSFileName(x); Delete(x, 1, length(nume)+1); Delete(x, pos(' '+nume, x), length(nume)+1);
  locI:=-1; OraseAcum:=false;
  XML.Root.ChildNodes[judI].AddChild('Comune');
  with XML.Root.ChildNodes[judI].Find('Comune') do
  	begin SetAttribute('pop', x) end
end;

procedure GetTownOrVillage(x: string);
var nume: string; node: TXMLNode;
begin
  nume:=GetXLSFileName(x); Delete(x, 1, length(nume)+1); Delete(x, pos(' '+nume, x), length(nume)+1);
  inc(locI);
  if OraseAcum then node:=XML.Root.ChildNodes[judI].Find('Municipii_Orase') else node:=XML.Root.ChildNodes[judI].Find('Comune');
  with node.AddChild('localitate') do
		begin SetAttribute('nume', nume); SetAttribute('pop', x) end
end;

function ConvertDataFile(inF, outF: string): boolean;
var s: TStringList; sx: string; ps: word;
begin
  try
		Result:=False; s:=TStringList.Create; s.LoadFromFile(inF); XML:=TXMLVerySimple.Create; ps:=0;
    with XML.Root do begin NodeName:='Recensamant2011' end;
    // Clean-up
    try for ps:=0 to s.Count-1 do begin sx:=s[ps]; StrCleanup(sx, true, true); s[ps]:=AnsiUpperCase(sx) end
    except showmessagefmt('phase "string cleanup" ERROR: line i=%d', [ps]) end; s.SaveToFile('temp.txt');
    // Formatting
    s.Insert(0, ''); s.Insert(s.Count, '');
    for ps:=1 to s.Count-2 do
    	if (pos('.', s[ps])=0) and (s[ps]<>'') and (s[ps-1]='') and (s[ps+1]='') then s[ps]:='#'+s[ps];
    ps:=0; while ps<s.Count do if s[ps]='' then s.Delete(ps) else inc(ps);
    s.SaveToFile('temp.txt');
		// Extraction
    ps:=0; judI:=-1;
    while ps < s.Count do
    	begin
        if s[ps][1]='#' then GetNewJudet(s[ps])
        else
        	case StrToCase(copy(s[ps], 1, 2), ['A.', 'B.']) of
          0: PrepareTowns(s[ps]);
          1: PrepareVillages(s[ps]);
          else GetTownOrVillage(s[ps])
          end;
        inc(ps)
      end;
    XML.SaveToFile(outF); XML.Free; Result:=True
  except Result:=False; Exit end
end;

end.
