﻿unit SelectorJudete;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls;

const
  nRow=11;
	nCol=4;

type
  TFSelJud = class(TForm)
    bg: TImage;
    CloseB: TImage;
    Panel1: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    SelToateB: TImage;
    SelNoneB: TImage;
    SelNumeB: TImage;
    Edit1: TEdit;
    procedure CloseBMouseEnter(Sender: TObject);
    procedure CloseBMouseLeave(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CloseBClick(Sender: TObject);
    procedure SelToateBClick(Sender: TObject);
    procedure SelNoneBClick(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    procedure RefreshSelJudInfo;
  end;

var
  FSelJud: TFSelJud;
  ch: array of array of TImage;
  n: word;

implementation

{$R *.dfm}

uses DataTypes, CustomVCL, Main;

procedure TFSelJud.CloseBMouseEnter(Sender: TObject);
begin RefreshImageButton(TImage(Sender), ControlColorMouse, false) end;

procedure TFSelJud.CloseBMouseLeave(Sender: TObject);
begin RefreshImageButton(TImage(Sender), ControlColor, false) end;

procedure TFSelJud.FormCreate(Sender: TObject);
var i, j, k: Integer;
begin
	with bg.Canvas do begin Brush.Color:=FormColor; Pen.Color:=$00C6D7DF; Pen.Width:=10; Rectangle(0, 0, bg.Width, bg.Height) end;
	SelToateB.OnMouseEnter:=FMain.SelJudBMouseEnter; SelToateB.OnMouseLeave:=FMain.SelJudBMouseLeave;
  SelNoneB.OnMouseEnter:=FMain.SelJudBMouseEnter; SelNoneB.OnMouseLeave:=FMain.SelJudBMouseLeave;
  SelNumeB.OnMouseEnter:=FMain.SelJudBMouseEnter; SelNumeB.OnMouseLeave:=FMain.SelJudBMouseLeave;
  SelToateB.OnMouseLeave(SelToateB); SelNoneB.OnMouseLeave(SelNoneB); SelNumeB.OnMouseLeave(SelNumeB);
  k:=-1; SetLength(ch, nCol);
	for i:=0 to nCol-1 do
  	begin
    	SetLength(ch[i], nRow);
	  	for j:=0 to nRow-1 do
   		 	begin
 		     	Inc(k); if k>=d.nJudete then Break;
	        ch[i, j]:=TImage.Create(Self); ch[i, j].Parent:=Panel1; ch[i, j].Cursor:=crHandPoint;
	        ch[i, j].Left:=i*180; ch[i, j].Top:=j*24; ch[i, j].Height:=24; ch[i, j].Width:=180;
	        ch[i, j].OnMouseEnter:=FMain.CheckBoxImgMouseEnter;
	        ch[i, j].OnMouseLeave:=FMain.CheckBoxImgMouseLeave;
	        ch[i, j].OnClick:=FMain.CheckBoxImgClick;
	        ch[i, j].Hint:='0'+d.Judet[k].OwnData.Nume
	      end
    end
end;

procedure TFSelJud.FormShow(Sender: TObject);
var i, j, k: Integer; s: string;
begin
	CloseB.OnMouseLeave(CloseB); Edit1.Text:='';
  for i:=0 to nCol-1 do
  	for j:=0 to nRow-1 do
      if ch[i, j]<>nil then
    	begin
      	s:=Copy(ch[i, j].Hint, 2, 255); //showmessagefmt('i=%d/%d, j=%d/%d, jud=%s', [i, nCol, j, nRow, s]);
			  ch[i, j].Hint:=Format('%d%s', [Integer(FiltruJudete.IndexOf(s)<>-1), s]);
        ch[i, j].OnMouseLeave(ch[i, j])
      end;
  RefreshSelJudInfo
end;

procedure TFSelJud.SelToateBClick(Sender: TObject);
var i, j: word;
begin
  Edit1.Text:='';
	for i:=0 to nCol-1 do for j:=0 to nRow-1 do if ch[i, j]<>nil then
  	begin ch[i, j].Hint:='1'+Copy(ch[i, j].Hint, 2, 255); ch[i, j].OnMouseLeave(ch[i, j]) end;
  RefreshSelJudInfo
end;

procedure TFSelJud.SelNoneBClick(Sender: TObject);
var i, j: word;
begin
  Edit1.Text:='';
	for i:=0 to nCol-1 do for j:=0 to nRow-1 do if ch[i, j]<>nil then
  	begin ch[i, j].Hint:='0'+Copy(ch[i, j].Hint, 2, 255); ch[i, j].OnMouseLeave(ch[i, j]) end;
  RefreshSelJudInfo
end;

procedure TFSelJud.Edit1Change(Sender: TObject);
var i, j: word;
begin
  for i:=0 to nCol-1 do for j:=0 to nRow-1 do if ch[i, j]<>nil then
  	begin ch[i, j].Hint:=Format('%d%s', [Integer(pos(AnsiUpperCase(Edit1.Text), AnsiUpperCase(Copy(ch[i, j].Hint, 2, 255)))<>0), Copy(ch[i, j].Hint, 2, 255)]); ch[i, j].OnMouseLeave(ch[i, j]) end;
  RefreshSelJudInfo
end;

procedure TFSelJud.RefreshSelJudInfo;
var i, j: word;
begin
  n:=0; for i:=0 to nCol-1 do for j:=0 to nRow-1 do if ch[i, j]<>nil then Inc(n, Integer(ch[i, j].Hint[1]='1'));
  Label3.Caption:=Format('Filtru județe (%d/%d)', [n, d.nJudete])
end;

procedure TFSelJud.CloseBClick(Sender: TObject);
var i, j: word;
begin
  FiltruJudete.Clear;
  for i:=0 to nCol-1 do for j:=0 to nRow-1 do if ch[i, j]<>nil then if ch[i, j].Hint[1]='1' then
  	FiltruJudete.Add(Copy(ch[i, j].Hint, 2, 255));
	FSelJud.Close
end;

procedure TFSelJud.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	FMain.RecompileList
end;

end.
