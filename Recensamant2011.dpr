program Recensamant2011;

uses
  Vcl.Forms,
  Main in 'Main.pas' {FMain},
  DataTypes in 'DataTypes.pas',
  Convertor in 'Convertor.pas',
  CustomVCL in 'CustomVCL.pas',
  TipInfo in 'TipInfo.pas' {FTipInfo},
  About in 'About.pas' {FAbout},
  SelectorJudete in 'SelectorJudete.pas' {FSelJud};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFMain, FMain);
  Application.CreateForm(TFTipInfo, FTipInfo);
  Application.CreateForm(TFAbout, FAbout);
  Application.CreateForm(TFSelJud, FSelJud);
  Application.Run;
end.
