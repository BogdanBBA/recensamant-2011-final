object FSelJud: TFSelJud
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'FSelJud'
  ClientHeight = 513
  ClientWidth = 808
  Color = 11379597
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object bg: TImage
    Left = 16
    Top = 16
    Width = 777
    Height = 481
  end
  object CloseB: TImage
    Left = 300
    Top = 417
    Width = 200
    Height = 56
    Cursor = crHandPoint
    Hint = 'OK'
    OnClick = CloseBClick
    OnMouseEnter = CloseBMouseEnter
    OnMouseLeave = CloseBMouseLeave
  end
  object Label3: TLabel
    Left = 40
    Top = 32
    Width = 142
    Height = 37
    Caption = 'Filtru jude'#539'e'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 40
    Top = 64
    Width = 730
    Height = 37
    Caption = 
      'Selecteaz'#259' din care jude'#539'e vor fi selectate localit'#259#539'i pentru af' +
      'i'#537'are:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Segoe UI Light'
    Font.Style = []
    ParentFont = False
  end
  object SelToateB: TImage
    Left = 40
    Top = 107
    Width = 180
    Height = 24
    Cursor = crHandPoint
    Hint = 'Selecteaz'#259' toate'
    OnClick = SelToateBClick
  end
  object SelNoneB: TImage
    Left = 226
    Top = 107
    Width = 180
    Height = 24
    Cursor = crHandPoint
    Hint = 'Selecteaz'#259' niciunul'
    OnClick = SelNoneBClick
  end
  object SelNumeB: TImage
    Left = 412
    Top = 107
    Width = 180
    Height = 24
    Hint = 'Selecteaz'#259' care con'#539'in:'
  end
  object Panel1: TPanel
    Left = 40
    Top = 137
    Width = 720
    Height = 264
    BevelOuter = bvNone
    TabOrder = 0
  end
  object Edit1: TEdit
    Left = 598
    Top = 104
    Width = 172
    Height = 28
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Text = 'Edit1'
    OnChange = Edit1Change
  end
end
