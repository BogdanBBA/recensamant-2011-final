object FAbout: TFAbout
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'FAbout'
  ClientHeight = 304
  ClientWidth = 546
  Color = 11379597
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object bg: TImage
    Left = 16
    Top = 16
    Width = 513
    Height = 273
  end
  object Label3: TLabel
    Left = 44
    Top = 40
    Width = 329
    Height = 37
    Caption = 'Recens'#259'm'#226'nt 2011 / Final'
    Font.Charset = ANSI_CHARSET
    Font.Color = 14937840
    Font.Height = -27
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 44
    Top = 83
    Width = 153
    Height = 37
    Caption = 'by BogdyBBA'
    Font.Charset = ANSI_CHARSET
    Font.Color = 14937840
    Font.Height = -27
    Font.Name = 'Segoe UI Light'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 44
    Top = 147
    Width = 112
    Height = 37
    Caption = 'v1.0 beta'
    Font.Charset = ANSI_CHARSET
    Font.Color = 14937840
    Font.Height = -27
    Font.Name = 'Segoe UI Semibold'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 196
    Top = 147
    Width = 280
    Height = 37
    Caption = '- July 6th and 8th, 2013'
    Font.Charset = ANSI_CHARSET
    Font.Color = 14937840
    Font.Height = -27
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object CloseB: TImage
    Left = 164
    Top = 208
    Width = 200
    Height = 56
    Cursor = crHandPoint
    Hint = #206'nchide'
    OnClick = CloseBClick
    OnMouseEnter = CloseBMouseEnter
    OnMouseLeave = CloseBMouseLeave
  end
end
