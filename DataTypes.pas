﻿unit DataTypes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UIntList, XML.VerySimple;

const
	nl=#13#10; dnl=nl+nl;

  TipPopulationFile: array[0..6] of string = ('ST', 'JUD', 'MUN', 'OR', 'COM', 'SECT', 'STATS');
  TipPopulation: array[0..7] of string = ('???', 'Stat', 'Județ', 'Municipiu', 'Oraș', 'Comună', 'Sector', 'Statistici');
  TipPopulationArticulated: array[0..7] of string = ('???', 'Statul', 'Județul', 'Municipiul', 'Orașul', 'Comuna', 'Sectorul', 'Statisticile');

  NPopItems=24; // Used to get the correct population values from the TPopulationEntity classes
  PopItem: array[0..NPopItems-1] of string = ('TOTAL', 'Romani', 'Maghiari', 'Romi', 'Ucraineni', 'Germani', 'Turci', 'Rusi-Lipoveni', 'Tatari',
  																					'Sarbi', 'Slovaci', 'Bulgari', 'Croati', 'Greci', 'Italieni', 'Evrei', 'Cehi', 'Polonezi',
                                            'Chinezi', 'Armeni', 'Ceangai', 'Macedoneni', 'Alta etnie', 'Informatie nedisponibila');

  NColumns=27; // Used to control the displayed columns in the program; only add at the end of the list, it's sortable in-program anyway
  OtherStr = '• • •';
  Column: array[0..NColumns-1] of string = ('Nr.', 'NUME', OtherStr, 'TOTAL', 'Romani', 'Maghiari', 'Romi', 'Ucraineni', 'Germani', 'Turci', 'Rusi-Lipoveni', 'Tatari',
  																					'Sarbi', 'Slovaci', 'Bulgari', 'Croati', 'Greci', 'Italieni', 'Evrei', 'Cehi', 'Polonezi',
                                            'Chinezi', 'Armeni', 'Ceangai', 'Macedoneni', 'Alta etnie', 'Informatie nedisponibila');
  ColumnWidth: array[0..NColumns-1] of word = (80, 240, 160, 120, 120, 120, 120, 120, 120, 120, 120, 120,
  																						 120, 120, 120, 120, 120, 120, 120, 120, 120,
  																						 120, 120, 120, 120, 120, 120);

type TStatistics=record
  totalLocalitati, totalMunicipii, totalOrase, totalComune: LongWord;
end;

type TPopulationEntity=class
  private
    Pop: array of LongWord;
    Asterisk: array of boolean;
  public
  	Nume: string;
    Tip: Byte;
    function GetPopulation(i: byte): LongWord;   overload;
    function GetPopulation(s: string): LongWord; overload;
    function GetAsterisk(i: byte): boolean;   overload;
    function GetAsterisk(s: string): boolean; overload;
    procedure InitializeValues;
    procedure AddValuesToCurrent(p: TPopulationEntity);
  	constructor Initialize(unNume, popS: string);
    destructor Free;
end;

type TJudet=record
	OwnData: TPopulationEntity;
  nLocalitati: word;
  Localitate: array of TPopulationEntity;
end;

type TCensusData=class
  private
  	XML: TXMLVerySimple;
  public
  	nJudete: word;
  	Judet: array of TJudet;
    NationWideData, SelData: TPopulationEntity;
    Stats, SelStats: TStatistics;
  	constructor Create(fn: string);
    destructor Free;
    procedure ModifyAndSaveXMLtoTemp;
    procedure ExportToTXT;
    function GetJudetForLocality(s: string): TPopulationEntity;
end;

var
	d: TCensusData;
  TipInformatie: Char;

  SelCol, FiltruJudete: TStringList;
  SelColW: TIntList;

  List: TList;
  ListStart, ListEnd: Integer;

  SortingCriteria: string;
  SortingDescending: boolean;

procedure GetSelColWidths;
function ComparePopulationEntities(a, b: Pointer): integer;

implementation

uses Main, FunctiiBaza;

function GetColumnWidth(s: string): word;
var i: byte;
begin
  for i:=0 to NColumns-1 do if s=Column[i] then begin Result:=ColumnWidth[i]; Exit end;
  Result:=0
end;

procedure GetSelColWidths;
var tot, i, p, lastAdded: Integer;
begin
  tot:=FMain.MainPanel.Width; p:=0; lastAdded:=-1; SelColW.Clear;
  for i:=0 to SelCol.Count-1 do
  	if SelCol[i]=OtherStr then p:=i
    else begin SelColW.Add(GetColumnWidth(SelCol[i])); Inc(lastAdded); Inc(tot, -SelColW[lastAdded]) end;
  SelColW.Insert(p, tot)
end;

//

function TPopulationEntity.GetPopulation(i: byte): LongWord;
begin if not (i in [0..NPopItems-1]) then Result:=0 else Result:=Pop[i] end;

function TPopulationEntity.GetPopulation(s: string): LongWord;
var x: ShortInt; i: byte;
begin
	x:=-1; for i:=0 to NPopItems-1 do if s=PopItem[i] then begin x:=i; break end;
  if x=-1 then Result:=0 else Result:=Pop[x]
end;

function TPopulationEntity.GetAsterisk(i: byte): boolean;
begin if not (i in [0..NPopItems-1]) then Result:=False else Result:=Asterisk[i] end;

function TPopulationEntity.GetAsterisk(s: string): boolean;
var x: ShortInt; i: byte;
begin
	x:=-1; for i:=0 to NPopItems-1 do if s=PopItem[i] then begin x:=i; break end;
  if x=-1 then Result:=False else Result:=Asterisk[x]
end;

procedure TPopulationEntity.InitializeValues;
var i: byte;
begin for i:=0 to NPopItems-1 do Pop[i]:=0 end;

procedure TPopulationEntity.AddValuesToCurrent(p: TPopulationEntity);
var i: byte;
begin for i:=0 to NPopItems-1 do Pop[i] := Pop[i] + p.GetPopulation(i) end;

constructor TPopulationEntity.Initialize(unNume, popS: string);
var i: byte; sx: string;
begin
	//showmessagefmt('initialize(nume="%s", popS="%s")', [unNume, popS]);
  sx:=Copy(unNume, 1, pos('.', unNume)-1); Delete(unNume, 1, pos('.', unNume)+1);
  nume:=RightCase(unNume); Tip:=StrToCase(sx, TipPopulationFile)+1;
  //showmessagefmt('"%s" is "%s" (Tip=%d)', [nume, TipPopulation[Tip], Tip]);
  SetLength(Pop, NPopItems); SetLength(Asterisk, NPopItems);
  StrCleanup(popS, true, true); popS:=popS+' ';
	for i:=0 to NPopItems-1 do
  	begin
      sx:=Copy(popS, 1, pos(' ', popS)-1); Delete(popS, 1, length(sx)+1);
      if (sx='-') or (sx='*') then Asterisk[i]:=sx='*'
      else try Pop[i]:=StrToInt(sx) except MessageDlg('TPopulationEntity.Initialize() CONVERSION ERROR - substring "'+sx+'" from (truncated) string "'+popS+'"', mtError, [mbIgnore], 0) end
    end
end;

destructor TPopulationEntity.Free;
begin

end;

//

constructor TCensusData.Create(fn: string);
var i, j: word;
begin
  if not FileExists(fn) then MessageDlg('File "'+fn+'" does not exist!', mtError, [mbIgnore], 0);

  try XML:=TXMLVerySimple.Create; XML.LoadFromFile(fn)
  except on E:Exception do MessageDlg('XML initialization error:'+dnl+E.ClassName+' - '+E.Message, mtError, [mbIgnore], 0) end;

  NationWideData:=TPopulationEntity.Initialize('ST. Romania', '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0');
  SelData:=TPopulationEntity.Initialize('STATS. TOTAL', '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0'); SelData.Nume:='TOTAL';

  nJudete:=XML.Root.ChildNodes.Count; SetLength(Judet, nJudete);
  try
	  if nJudete>0 then for i:=0 to nJudete-1 do
	  	with XML.Root.ChildNodes[i] do
		  	begin
	  	    Judet[i].OwnData := TPopulationEntity.Initialize(Attribute['nume'], Attribute['pop']);

	        Judet[i].nLocalitati := ChildNodes.Count; SetLength(Judet[i].Localitate, Judet[i].nLocalitati);
          inc(Stats.totalLocalitati, Judet[i].nLocalitati);

	        if Judet[i].nLocalitati>0 then for j:=0 to Judet[i].nLocalitati-1 do
	        	with ChildNodes[j] do
            	begin
		          	Judet[i].Localitate[j] := TPopulationEntity.Initialize(Attribute['nume'], Attribute['pop']);
								case Judet[i].Localitate[j].Tip of 3: Inc(Stats.totalMunicipii); 4: Inc(Stats.totalOrase); 5: Inc(Stats.totalComune) end
              end
	    	end
  except MessageDlg('TCensusData.Create ERROR: i='+inttostr(i), mtError, [mbIgnore], 0) end;

  if nJudete>0 then for i:=0 to nJudete-1 do
    NationWideData.AddValuesToCurrent(Judet[i].OwnData)
end;

destructor TCensusData.Free;
var i, j: word;
begin
	NationWideData.Free;
  if nJudete>0 then for i:=0 to nJudete-1 do
	  begin
    	Judet[i].OwnData.Free;
  		if Judet[i].nLocalitati>0 then for j:=0 to Judet[i].nLocalitati-1 do Judet[i].Localitate[j].Free
    end;
  XML.Free
end;

procedure TCensusData.ModifyAndSaveXMLtoTemp;
	{*}procedure SetNodeAttributes(node: TXMLNode; ent: TPopulationEntity);
  //var i: word; s: string;
  begin
    {node.SetAttribute('nume', Format('%s. %s', [TipPopulationFile[ent.Tip-1], AnsiUpperCase(ent.Nume)]));
    s:=''; for i:=0 to NPopItems-1 do
    begin
    	if ent.GetPopulation(i)<>0 then s:=s+' '+IntToStr(ent.GetPopulation(i))
      else if not ent.GetAsterisk(i) then s:=s+' -' else s:=s+' *'
    end; Delete(s, 1, 1); node.SetAttribute('pop', s)}
  end;
//var i, j: word;
begin
  {XML.Free; XML:=TXMLVerySimple.Create; XML.Root.NodeName:='Recensamant2011';
	for i:=0 to d.nJudete-1 do
  	with XML.Root do
	  	begin
  	    AddChild('Judet'); SetNodeAttributes(ChildNodes.Last, d.Judet[i].OwnData);
        for j:=0 to d.Judet[i].nOrase-1 do
        	begin ChildNodes.Last.AddChild('localitate'); SetNodeAttributes(ChildNodes.Last.ChildNodes.Last, d.Judet[i].Oras[j]) end;
        for j:=0 to d.Judet[i].nComune-1 do
        	begin ChildNodes.Last.AddChild('localitate'); SetNodeAttributes(ChildNodes.Last.ChildNodes.Last, d.Judet[i].Comuna[j]) end
    	end;
  XML.SaveToFile('tempXML.xml')}
end;

procedure TCensusData.ExportToTXT;
var f: TextFile;
begin
	AssignFile(f, 'Text Export.txt'); ReWrite(f);
	//for
  CloseFile(f)
end;

function TCensusData.GetJudetForLocality(s: string): TPopulationEntity;
var i, j: integer;
begin
	result:=nil;
  if nJudete>0 then for i:=0 to nJudete-1 do
  	if Judet[i].nLocalitati>0 then for j:=0 to Judet[i].nLocalitati-1 do
    	if Judet[i].Localitate[j].Nume=s then begin result:=Judet[i].OwnData; break end
end;

//

function ComparePopulationEntities(a, b: Pointer): integer;
var x, y: TPopulationEntity;
begin
	x:=TPopulationEntity(a); y:=TPopulationEntity(b);
  if SortingCriteria='NUME' then
  	begin
      if x.Nume>y.Nume then Result:=-1
      else if x.Nume<y.Nume then Result:=1
      else Result:=0
    end
  else
  	begin
      if (x.GetPopulation(SortingCriteria)<>0) or (y.GetPopulation(SortingCriteria)<>0) then
      	begin
		      if x.GetPopulation(SortingCriteria)>y.GetPopulation(SortingCriteria) then Result:=-1
		      else if x.GetPopulation(SortingCriteria)<y.GetPopulation(SortingCriteria) then Result:=1
		      else Result:=0
        end
      else
      	begin
          if x.GetAsterisk(SortingCriteria)>y.GetAsterisk(SortingCriteria) then Result:=-1
          else if x.GetAsterisk(SortingCriteria)<y.GetAsterisk(SortingCriteria) then Result:=1
          else Result:=0
        end
    end;
  if not SortingDescending then Result:=-Result
end;

end.
